using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    [SerializeField] Transform _start;
    [SerializeField] Transform _end;
    [SerializeField] Transform _sprite;
    [SerializeField] float _speed = .5f;

    float positionPercent;
    int direction = 1;

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(_start.position, _end.position);
        float speedForDistance = _speed / distance;

        positionPercent += Time.deltaTime * direction * speedForDistance;

        _sprite.position = Vector3.Lerp(_start.position, _end.position, positionPercent);

        if (positionPercent >= 1 && direction == 1)
            direction = -1;
        else if (positionPercent <= 0 && direction == -1)
            direction = 1;
    }
}
