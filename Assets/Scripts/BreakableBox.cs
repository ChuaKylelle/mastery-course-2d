using UnityEngine;

public class BreakableBox : MonoBehaviour, ITakeShellHit
{
    public void HandleShellHits(ShellFlipped shellFlipped)
    {
        Destroy(gameObject);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.WasHitByPlayer() && collision.HitFromBelow())
        {
            Destroy(gameObject);
        }
    }
}
