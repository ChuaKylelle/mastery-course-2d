using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UILives : MonoBehaviour
{
    TextMeshProUGUI textMeshProUGUI;

    private void Awake()
    {
        textMeshProUGUI = GetComponent<TextMeshProUGUI>();
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.OnHealthChanged += HandleOnHealthChanged;
        textMeshProUGUI.SetText(GameManager.Instance.Lives.ToString());
    }

    void OnDestroy()
    {
        GameManager.Instance.OnHealthChanged -= HandleOnHealthChanged;
    }

    void HandleOnHealthChanged(int livesRemaining)
    {
        textMeshProUGUI.SetText(livesRemaining.ToString());
    }
}
