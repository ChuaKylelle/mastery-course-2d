using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterGrounding : MonoBehaviour
{
    [SerializeField] Transform[] _positions;
    [SerializeField] float _maxDistance;
    [SerializeField] LayerMask _layerMask;

    Transform groundedObject;
    Vector3? groundedObjectLastPosition; // "?" makes it nullable

    public bool IsGrounded { get; private set; }
    public Vector2 GroundedDirection { get; private set; }


    // Update is called once per frame
    void Update()
    {
        foreach (var position in _positions)
        {
            CheckFootIfGrounded(position);
            if (IsGrounded)
                break;
        }

        StickingToMovingObject();
    }

    void StickingToMovingObject()
    {
        if(groundedObject != null)
        {
            if (groundedObjectLastPosition.HasValue &&
                groundedObjectLastPosition.Value != groundedObject.position)
            {
                Vector3 delta = groundedObject.position - groundedObjectLastPosition.Value;
                Debug.Log(delta);
                transform.position += delta;
            }
            groundedObjectLastPosition = groundedObject.position;
        }
        else
        {
            groundedObjectLastPosition = null;
        }
    }

    void CheckFootIfGrounded(Transform foot)
    {
        var raycastHit = Physics2D.Raycast(foot.position, foot.forward, _maxDistance, _layerMask);
        Debug.DrawRay(foot.position, foot.forward * _maxDistance, Color.red);

        if (raycastHit.collider != null)
        {
            if(groundedObject != raycastHit.collider.transform)
            {
                groundedObject = raycastHit.collider.transform;
                IsGrounded = true;
                groundedObjectLastPosition = raycastHit.collider.transform.position;
                GroundedDirection = foot.forward;
            }
           
        }
        else
        {
            groundedObject = null;
            IsGrounded = false;
        }
           
    }
}
