using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShellFlipped : MonoBehaviour
{
    [SerializeField] float _shellSpeed;

    Vector2 direction;

    new Collider2D collider2D;
    new Rigidbody2D rigidbody2D;

    private void Awake()
    {
        collider2D = GetComponent<Collider2D>();
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        rigidbody2D.velocity = new Vector2(direction.x * _shellSpeed, rigidbody2D.velocity.y);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.WasHitByPlayer())
        {
            HandlePlayerCollision(collision);
        }
        else
        {
            if (collision.HitFromSide())
            {
                LaunchShell(collision);
                var takeShellHit = collision.collider.GetComponent<ITakeShellHit>();
                if(takeShellHit != null)
                {
                    takeShellHit.HandleShellHits(this);
                }
            }
                
        }
    }

    private void HandlePlayerCollision(Collision2D collision)
    {
        var player = collision.collider.GetComponent<PlayerMovementController>();

        if (direction.magnitude == 0)
        {
            LaunchShell(collision);
            if(collision.HitFromTop())
                player.Bounce();
        }
        else
        {
            if (collision.HitFromTop())
            {
                direction = Vector2.zero;
                player.Bounce();
            }
            else
            {
                GameManager.Instance.KillPlayer();
            }
        }
    }

    void LaunchShell(Collision2D collision)
    {
        var floatDirection = collision.contacts[0].normal.x > 0 ? 1f : -1f;
        direction = new Vector2(floatDirection, 0);
    }
}
