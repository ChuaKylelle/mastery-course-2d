using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walker : MonoBehaviour, ITakeShellHit
{
    [SerializeField] float _speed = 5f;
    [SerializeField] GameObject _spawnOnStompPrefab;

    SpriteRenderer spriteRenderer;
    new Collider2D collider;
    new Rigidbody2D rigidbody2D;
    Vector2 direction = Vector2.left;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        collider = GetComponent<Collider2D>();
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.WasHitByPlayer())
        {
            if (collision.HitFromTop())
                HandleWalkerStomp(collision.collider.GetComponent<PlayerMovementController>());
            else
                GameManager.Instance.KillPlayer();
        }
    }

    private void HandleWalkerStomp(PlayerMovementController player)
    {
        if(_spawnOnStompPrefab != null)
            Instantiate(_spawnOnStompPrefab, transform.position, transform.rotation);

        player.Bounce();

        Destroy(gameObject);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rigidbody2D.MovePosition(rigidbody2D.position + direction * _speed * Time.deltaTime);
    }

    void LateUpdate()
    {
        if (ReachedEdge() || HitNotPlayer())
        {
            SwitchDirection();
        }
    }

    private bool HitNotPlayer()
    {
        float offset = 0.1f;
        float x = GetForwardX(offset);
        float y = transform.position.y;

        Vector2 origin = new Vector2(x, y);
        Debug.DrawRay(origin, direction * 0.1f);

        var hit = Physics2D.Raycast(origin, direction, 0.1f);

        if (hit.collider == null)
            return false;

        if (hit.collider.isTrigger)
            return false;

        if (hit.collider.GetComponent<PlayerMovementController>())
            return false;

        return true;


    }

    private bool ReachedEdge()
    {
        float offset = 0.1f;
        float x = GetForwardX(offset);

        float y = collider.bounds.min.y;

        Vector2 origin = new Vector2(x, y);
        Debug.DrawRay(origin, Vector2.down * 0.1f);

        var hit = Physics2D.Raycast(origin, Vector2.down, 0.1f);

        if (hit.collider == null)
            return true;

        return false;
    }

    private float GetForwardX(float offset)
    {
        return direction.x == -1 ? collider.bounds.min.x - offset : collider.bounds.max.x + offset;
    }

    private void SwitchDirection()
    {
        direction *= -1;
        spriteRenderer.flipX = !spriteRenderer.flipX;
    }

    public void HandleShellHits(ShellFlipped shellFlipped)
    {
        Destroy(gameObject);
    }
}
