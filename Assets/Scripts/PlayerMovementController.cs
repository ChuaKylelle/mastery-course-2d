using System;
using UnityEngine;

[RequireComponent(typeof(CharacterGrounding))]
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovementController : MonoBehaviour, IMove
{
    [SerializeField] float _speed = 5f;
    [SerializeField] float _jumpForce = 400;
    [SerializeField] float _bounceForce = 300;
    [SerializeField] float _wallJumpForce = 300;

    Rigidbody2D _rigidbody;
    CharacterGrounding _characterGrounding;

    public float Speed { get; private set; }

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _characterGrounding = GetComponent<CharacterGrounding>();
        

    }

    void Update()
    {
        if (Input.GetButtonDown("Jump") && _characterGrounding.IsGrounded)
        {
            Jump();
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        Speed = horizontal;

        Vector3 movement = new Vector3(horizontal, 0);

        transform.position += (movement * _speed) * Time.deltaTime;
        //_rigidbody.AddForce((movement * _speed));

        
    }

    void Jump()
    {
        Debug.Log("Jump");
        //rigidbody.velocity = Vector2.up * _jumpForce;
        _rigidbody.AddForce(Vector2.up * _jumpForce);

        if(_characterGrounding.GroundedDirection != Vector2.down)
        {
            _rigidbody.AddForce(_characterGrounding.GroundedDirection * -1f * _wallJumpForce);
            
        }
    }

    internal void Bounce()
    {
        _rigidbody.AddForce(Vector2.up * _bounceForce);
    }
}
