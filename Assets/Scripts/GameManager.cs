using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public int Lives { get; private set; }

    public event Action<int> OnHealthChanged;
    public event Action<int> OnCoinChanged;


    int _coins;
    int currentLevelIndex;

    // void Update()
    //{
    //    System.Threading.Thread.Sleep(50);
    //}

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(Instance);

            RestartGame();
        }
        else
        {
            Destroy(gameObject);
        }
    }


    public void KillPlayer()
    {
        Lives--;
        OnHealthChanged?.Invoke(Lives);

        if(Lives <= 0)
        {
            RestartGame();
        }
        else
        {
            SendPlayerToCheckpoint();
        }

    }

    private void SendPlayerToCheckpoint()
    {
        var checkpointManager = FindObjectOfType<CheckpointManager>();

        var checkpoint = checkpointManager.GetRecentActivateCheckpoint();

        var player = FindObjectOfType<PlayerMovementController>();

        player.transform.position = checkpoint.transform.position;

        
    }

    public void ProceedToNextLevel()
    {
        currentLevelIndex++;
        SceneManager.LoadScene(currentLevelIndex);
    }

    public void AddCoin()
    {
        _coins++;
        OnCoinChanged?.Invoke(_coins);
    }

    private void RestartGame()
    {
        Lives = 3;
        _coins = 0;

        OnCoinChanged?.Invoke(_coins);
        SceneManager.LoadScene(0);
    }
}
