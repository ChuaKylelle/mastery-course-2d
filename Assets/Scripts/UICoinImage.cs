using UnityEngine;

public class UICoinImage : MonoBehaviour
{
    Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        GameManager.Instance.OnCoinChanged += Pulse;
    }

    void OnDestroy()
    {
        GameManager.Instance.OnCoinChanged -= Pulse;
    }

    void Pulse(int coins)
    {
        animator.SetTrigger("Pulse");
      
    }

   

}
