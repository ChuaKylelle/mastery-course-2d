using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UICoins : MonoBehaviour
{
    TextMeshProUGUI textMeshPro;

    void Awake()
    {
        textMeshPro = GetComponent<TextMeshProUGUI>();
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.OnCoinChanged += HandleOnCoinChanged;

    }

    void OnDestroy()
    {
        GameManager.Instance.OnCoinChanged -= HandleOnCoinChanged;
    }

    void HandleOnCoinChanged(int coins)
    {
        textMeshPro.SetText(coins.ToString());
    }

    
}
