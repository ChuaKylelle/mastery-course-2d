using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBox : MonoBehaviour, ITakeShellHit
{
    [SerializeField] SpriteRenderer enabledSprite;
    [SerializeField] SpriteRenderer disabledSprite;
    [SerializeField] int totalCoins = 1;

    int remainingCoins;
    Animator anim;

    public void HandleShellHits(ShellFlipped shellFlipped)
    {
        if(remainingCoins > 0)
            TakeCoins();
    }

    void Awake()
    {
        remainingCoins = totalCoins;
        disabledSprite.enabled = false;
        anim = GetComponent<Animator>();
    }


    void OnCollisionEnter2D(Collision2D collision)
    {

        if (remainingCoins > 0 && collision.WasHitByPlayer() && collision.HitFromBelow())
        {
            Debug.Log(collision.contacts[0].normal);

            TakeCoins();
        }
    }

    private void TakeCoins()
    {
        GameManager.Instance.AddCoin();
        anim.SetTrigger("Flip");
        remainingCoins--;

        if (remainingCoins <= 0)
        {
            enabledSprite.enabled = false;
            disabledSprite.enabled = true;

        }
    }
}
