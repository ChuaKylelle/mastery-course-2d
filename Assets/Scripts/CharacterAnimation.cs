using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(SpriteRenderer))]
public class CharacterAnimation : MonoBehaviour
{
    Animator _animator;
    SpriteRenderer _spriteRenderer;
    IMove _moveController;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _moveController = GetComponent<IMove>();
    }

    void Update()
    {
        float speed = _moveController.Speed;
        _animator.SetFloat("Speed", Mathf.Abs(speed));

        if(speed != 0)
            _spriteRenderer.flipX = speed < 0;
    }

}
