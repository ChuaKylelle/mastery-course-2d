using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinAudio : MonoBehaviour
{
    AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.OnCoinChanged += PlayCoinAudio;
    }

    void OnDestroy()
    {
        GameManager.Instance.OnCoinChanged -= PlayCoinAudio;
    }


    void PlayCoinAudio(int coins)
    {
        audioSource.Play();
    }

   

}
